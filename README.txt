#####################################################################################################################
#                                  imagInfinito - Trabalho 2 de Computação Gráfica 
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação
#	  2.1. Classes
#   3. Resultados e Conclusão
#   
#   @autor Aline Rodrigues Tonini
#   @version 2.0
#   @ 13/05/2014
#####################################################################################################################
#   1. Introduçao
#####################################################################################################################
#
#   O projeto imagInfinito se propõem a realizar o trabalho 2 de Computação Gráfica, proposto pelo Prof. Marilton Aguiar.
#   Tendo como base o trabalho 1. Com o algoritmo do ponto médio para desenho de círculos, calcular uma trajetória da metade 
#   inferior do símbolo de infinito. Esta lista dos pontos calculados pelo algoritmo será dividida apropriadamente
#   em 8 segmentos.
#
#   A cada oitavo, de cada semi-círculo, utilize uma imagem diferente. Foi utilizado imagens bufferizadas e 
#   a interpolação de Shapes para implementar a troca suave entre as imagens combinando apropriadamente cinco 
#   pontos de interesse em cada imagem.
#
#   Implementado utilizando o Java 2D.	        
#####################################################################################################################
#   2. Implementação
#####################################################################################################################
#
#    Na implementação do Projeto utilizou -se Java2D e os paradigmas da  orientação a objetos. 
#    O trabalho foi desenvolvido utilizando a IDE Netbeans e o versionador Bitbucket para manter
#    o código organizado.
#
#    Foram utilizadas imagens de orquídeas para o trabalho, motivo : gosto pessoal.
#
#    A implementação foi realizada utilizando  uma parte do trabalho 1, onde era realizado o calculo dos pontos com
#    a equação do ponto médio. Foi usado a combinação convexa para calcular os steps da imagem a partir do pontos
#    dos octetos do círculo. Tendo esses pontos dos steps a imagem é mostrada na tela e em cada octeto há uma troca de
#    imagens.
#
#   2.1 Classes do Projeto
#        Classes: 
#		Classe Main: Classe onde é retirado os dados necessários (R e nº de steps) e inicializado as a tela principal.
#		Classe MorphRet : Classe onde as imagens são definidas e onde são definidos seus pontos de interesse e 
#				os triangulos para realizar a transformação entre elas.
#				Nessa classe também é implementado os metodos que calculam a trajetoria da imagem e o 
#				método run para realizar a transformção.
#		Classe BufferedImageDrawer: Armazena as imagens em um buffer.
#		Classe TriangulatedImage: Faz a triangulação das imagens.
#		Classe Coordenada : Essa classe é usada para implementar as coordenadas de cada ponto.
#		Classe MyFinishWindow: Implementa um listener para fechar a janela da animação com
#				       com um clique.
#
#####################################################################################################################
# 	3. Resultados e Conclusão
#####################################################################################################################
#
#       O resultado da execução é a animação de uma imagem 240x200 que vai sendo transformada em outra a cada
#	octeto do semi-circulo.
#
#	Com este trabalho foi compreendido como funciona as animações em Java 2D, como fazer transformações do tipo
# 	Morph em imagens.
#
#####################################################################################################################