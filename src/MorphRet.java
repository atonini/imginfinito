
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.TimerTask;

/*
 * Autor : Aline Tonini
 * Data 13/05/2014
 * 
 * */
public class MorphRet extends TimerTask {

    private int altura;  // altura e largura da imagem
    private int largura;
    private int r;
    private int xi;
    private int yi;
    private double steps;
    private BufferedImageDrawer buffid;
    private TriangulatedImage t1, t2, t3, t4, t5, t6, t7, t8, t9;
    private BufferedImage mix;
    private double alpha;
    private double deltaAlpha;
    private ArrayList<Coordenada> trajetoria;
    
    //Constructor  
    public MorphRet(BufferedImageDrawer bid, int r, double steps) {
        this.altura = 200; 
        this.largura = 240;
        this.r = r;
        this.xi = r + largura / 2; //centro do circulo
        this.yi = r/2 + altura/2; //centro do circulo
        this.buffid = bid;
        this.steps = steps;
        this.deltaAlpha = 1.0 / steps;
        this.alpha = 0;
        this.trajetoria = new ArrayList<Coordenada>();
        //calculas todos os steps da trajetoria *os 2 semi-circulos
        calculaSteps();
             
        // for (Coordenada  a : trajetoria){
        //   System.out.println(a.getCoordenadaX() + "xx " +a.getCoordenadaY());
        // }
        Image loadedImage;

        t1 = new TriangulatedImage();
        t1.bi = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2dt1 = t1.bi.createGraphics();
        loadedImage = new javax.swing.ImageIcon("imagem1.jpg").getImage();
        g2dt1.drawImage(loadedImage, 0, 0, null);

        t1.tPoints = new Point2D[13];
        t1.tPoints[0] = new Point2D.Double(0, 0);
        t1.tPoints[1] = new Point2D.Double(0, 50);
        t1.tPoints[2] = new Point2D.Double(0, 199);
        t1.tPoints[3] = new Point2D.Double(55, 120);
        t1.tPoints[4] = new Point2D.Double(88, 40);
        t1.tPoints[5] = new Point2D.Double(115, 0);
        t1.tPoints[6] = new Point2D.Double(115, 100);
        t1.tPoints[7] = new Point2D.Double(155, 199);
        t1.tPoints[8] = new Point2D.Double(140, 30);
        t1.tPoints[9] = new Point2D.Double(190, 120);
        t1.tPoints[10] = new Point2D.Double(239, 0);
        t1.tPoints[11] = new Point2D.Double(239, 40);
        t1.tPoints[12] = new Point2D.Double(239, 199);

        t1.triangles = new int[16][3];
        t1.triangles[0][0] = 0;
        t1.triangles[0][1] = 1;
        t1.triangles[0][2] = 4;
        t1.triangles[1][0] = 0;
        t1.triangles[1][1] = 4;
        t1.triangles[1][2] = 5;
        t1.triangles[2][0] = 1;
        t1.triangles[2][1] = 4;
        t1.triangles[2][2] = 3;
        t1.triangles[3][0] = 4;
        t1.triangles[3][1] = 3;
        t1.triangles[3][2] = 6;
        t1.triangles[4][0] = 4;
        t1.triangles[4][1] = 6;
        t1.triangles[4][2] = 5;
        t1.triangles[5][0] = 1;
        t1.triangles[5][1] = 2;
        t1.triangles[5][2] = 3;
        t1.triangles[6][0] = 2;
        t1.triangles[6][1] = 3;
        t1.triangles[6][2] = 7;
        t1.triangles[7][0] = 3;
        t1.triangles[7][1] = 7;
        t1.triangles[7][2] = 6;
        t1.triangles[8][0] = 5;
        t1.triangles[8][1] = 8;
        t1.triangles[8][2] = 10;
        t1.triangles[9][0] = 5;
        t1.triangles[9][1] = 6;
        t1.triangles[9][2] = 8;
        t1.triangles[10][0] = 8;
        t1.triangles[10][1] = 10;
        t1.triangles[10][2] = 11;
        t1.triangles[11][0] = 6;
        t1.triangles[11][1] = 8;
        t1.triangles[11][2] = 9;
        t1.triangles[12][0] = 8;
        t1.triangles[12][1] = 11;
        t1.triangles[12][2] = 9;
        t1.triangles[13][0] = 6;
        t1.triangles[13][1] = 7;
        t1.triangles[13][2] = 9;
        t1.triangles[14][0] = 11;
        t1.triangles[14][1] = 12;
        t1.triangles[14][2] = 9;
        t1.triangles[15][0] = 7;
        t1.triangles[15][1] = 9;
        t1.triangles[15][2] = 12;

        t2 = new TriangulatedImage();
        t2.bi = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);

        Graphics2D g2dt2 = t2.bi.createGraphics();
        loadedImage = new javax.swing.ImageIcon("imagem2.jpg").getImage();
        g2dt2.drawImage(loadedImage, 0, 0, null);

        t2.tPoints = new Point2D[13];
        t2.tPoints[0] = new Point2D.Double(0, 0);
        t2.tPoints[1] = new Point2D.Double(0, 90);
        t2.tPoints[2] = new Point2D.Double(0,199);
        t2.tPoints[3] = new Point2D.Double(56,35 );
        t2.tPoints[4] = new Point2D.Double(41,140);
        t2.tPoints[5] = new Point2D.Double(110, 0);
        t2.tPoints[6] = new Point2D.Double(110, 100);
        t2.tPoints[7] = new Point2D.Double(110, 199);
        t2.tPoints[8] = new Point2D.Double(140, 30);
        t2.tPoints[9] = new Point2D.Double(190, 100);
        t2.tPoints[10] = new Point2D.Double(239, 0);
        t2.tPoints[11] = new Point2D.Double(239, 100);
        t2.tPoints[12] = new Point2D.Double(239, 199);
       
        t2.triangles = t1.triangles;

         t3= new TriangulatedImage();
         t3.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt3 = t3.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem3.jpg").getImage();
         g2dt3.drawImage(loadedImage,0,0,null);

         t3.tPoints = new Point2D[13];
         t3.tPoints[0] = new Point2D.Double(0,0);
         t3.tPoints[1] = new Point2D.Double(0,110);
         t3.tPoints[2] = new Point2D.Double(0,199);
         t3.tPoints[3] = new Point2D.Double(78,78);
         t3.tPoints[4] = new Point2D.Double(70,136);
         t3.tPoints[5] = new Point2D.Double(112,0);
         t3.tPoints[6] = new Point2D.Double(112,104);
         t3.tPoints[7] = new Point2D.Double(112,199);
         t3.tPoints[8] = new Point2D.Double(150,75);
         t3.tPoints[9] = new Point2D.Double(158,126);
         t3.tPoints[10] = new Point2D.Double(239,0);
         t3.tPoints[11] = new Point2D.Double(239,104);
         t3.tPoints[12] = new Point2D.Double(239,199);

         t3.triangles = t1.triangles;

         t4= new TriangulatedImage();
         t4.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt4 = t4.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem4.jpg").getImage();
         g2dt4.drawImage(loadedImage,0,0,null);

         t4.tPoints = new Point2D[13];
         t4.tPoints[0] = new Point2D.Double(0,0);
         t4.tPoints[1] = new Point2D.Double(0,120);
         t4.tPoints[2] = new Point2D.Double(0,199);
         t4.tPoints[3] = new Point2D.Double(60,40);
         t4.tPoints[4] = new Point2D.Double(54,140);
         t4.tPoints[5] = new Point2D.Double(120,0);
         t4.tPoints[6] = new Point2D.Double(120,120);
         t4.tPoints[7] = new Point2D.Double(120,199);
         t4.tPoints[8] = new Point2D.Double(195,50);
         t4.tPoints[9] = new Point2D.Double(200,120);
         t4.tPoints[10] = new Point2D.Double(239,0);
         t4.tPoints[11] = new Point2D.Double(239,120);
         t4.tPoints[12] = new Point2D.Double(239,199);

         t4.triangles = t1.triangles;

         t5= new TriangulatedImage();
         t5.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt5 = t5.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem5.jpg").getImage();
         g2dt5.drawImage(loadedImage,0,0,null);

         t5.tPoints = new Point2D[13];
         t5.tPoints[0] = new Point2D.Double(0,0);
         t5.tPoints[1] = new Point2D.Double(0,74);
         t5.tPoints[2] = new Point2D.Double(0,199);
         t5.tPoints[3] = new Point2D.Double(63,131);
         t5.tPoints[4] = new Point2D.Double(86,41);
         t5.tPoints[5] = new Point2D.Double(120,0);
         t5.tPoints[6] = new Point2D.Double(122,102);
         t5.tPoints[7] = new Point2D.Double(132,199);
         t5.tPoints[8] = new Point2D.Double(186,50);
         t5.tPoints[9] = new Point2D.Double(188,125);
         t5.tPoints[10] = new Point2D.Double(239,0);
         t5.tPoints[11] = new Point2D.Double(239,78);
         t5.tPoints[12] = new Point2D.Double(239,199);

         t5.triangles = t1.triangles;

         t6= new TriangulatedImage();
         t6.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt6 = t6.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem6.jpg").getImage();
         g2dt6.drawImage(loadedImage,0,0,null);

         t6.tPoints = new Point2D[13];
         t6.tPoints[0] = new Point2D.Double(0,0);
         t6.tPoints[1] = new Point2D.Double(0,37);
         t6.tPoints[2] = new Point2D.Double(0,199);
         t6.tPoints[3] = new Point2D.Double(27,116);
         t6.tPoints[4] = new Point2D.Double(70,25);
         t6.tPoints[5] = new Point2D.Double(110,0);
         t6.tPoints[6] = new Point2D.Double(118,108);
         t6.tPoints[7] = new Point2D.Double(120,199);
         t6.tPoints[8] = new Point2D.Double(177,20);
         t6.tPoints[9] = new Point2D.Double(214,107);
         t6.tPoints[10] = new Point2D.Double(239,0);
         t6.tPoints[11] = new Point2D.Double(239,34);
         t6.tPoints[12] = new Point2D.Double(239,199);

         t6.triangles = t1.triangles;

         t7= new TriangulatedImage();
         t7.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt7 = t7.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem7.jpg").getImage();
         g2dt7.drawImage(loadedImage,0,0,null);

         t7.tPoints = new Point2D[13];
         t7.tPoints[0] = new Point2D.Double(0,0);
         t7.tPoints[1] = new Point2D.Double(0,107);
         t7.tPoints[2] = new Point2D.Double(0,199);
         t7.tPoints[3] = new Point2D.Double(42,42);
         t7.tPoints[4] = new Point2D.Double(54,118);
         t7.tPoints[5] = new Point2D.Double(116,0);
         t7.tPoints[6] = new Point2D.Double(116,100);
         t7.tPoints[7] = new Point2D.Double(116,199);
         t7.tPoints[8] = new Point2D.Double(206,55);
         t7.tPoints[9] = new Point2D.Double(200,117);
         t7.tPoints[10] = new Point2D.Double(239,0);
         t7.tPoints[11] = new Point2D.Double(239,80);
         t7.tPoints[12] = new Point2D.Double(239,199);

         t7.triangles = t1.triangles;

         t8= new TriangulatedImage();
         t8.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt8 = t8.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem8.jpg").getImage();
         g2dt8.drawImage(loadedImage,0,0,null);

         t8.tPoints = new Point2D[13];
         t8.tPoints[0] = new Point2D.Double(0,0);
         t8.tPoints[1] = new Point2D.Double(0,74);
         t8.tPoints[2] = new Point2D.Double(0,199);
         t8.tPoints[3] = new Point2D.Double(40,160);
         t8.tPoints[4] = new Point2D.Double(82,39);
         t8.tPoints[5] = new Point2D.Double(146,0);
         t8.tPoints[6] = new Point2D.Double(124,102);
         t8.tPoints[7] = new Point2D.Double(106,199);
         t8.tPoints[8] = new Point2D.Double(199,67);
         t8.tPoints[9] = new Point2D.Double(160,180);
         t8.tPoints[10] = new Point2D.Double(239,0);
         t8.tPoints[11] = new Point2D.Double(239,100);
         t8.tPoints[12] = new Point2D.Double(239,199);
         
         t8.triangles = t1.triangles;

         t9= new TriangulatedImage();
         t9.bi = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

         Graphics2D g2dt9 = t9.bi.createGraphics();
         loadedImage = new javax.swing.ImageIcon("imagem9.jpg").getImage();
         g2dt9.drawImage(loadedImage,0,0,null);

         t9.tPoints = new Point2D[13];
         t9.tPoints[0] = new Point2D.Double(0,0);
         t9.tPoints[1] = new Point2D.Double(0,102);
         t9.tPoints[2] = new Point2D.Double(0,199);
         t9.tPoints[3] = new Point2D.Double(90,50);
         t9.tPoints[4] = new Point2D.Double(75,160);
         t9.tPoints[5] = new Point2D.Double(121,0);
         t9.tPoints[6] = new Point2D.Double(135,118);
         t9.tPoints[7] = new Point2D.Double(140,199);
         t9.tPoints[8] = new Point2D.Double(188,40);
         t9.tPoints[9] = new Point2D.Double(202,141);
         t9.tPoints[10] = new Point2D.Double(239,0);
         t9.tPoints[11] = new Point2D.Double(239,76);
         t9.tPoints[12] = new Point2D.Double(239,199);

         t9.triangles = t1.triangles;
         
    }

    /*
     calcula a trajetoria da metade inferior do circulo de acordo com o raio.
    Detalhe: calcula a partir de (0,0) como origem
     */
    public static ArrayList<Coordenada> calcTrajetoria(double r) {

        ArrayList<Coordenada> octante1 = new ArrayList<Coordenada>();
        ArrayList<Coordenada> listaCoord = new ArrayList<Coordenada>();
        Coordenada aux;

        //cria trajetoria do circulo
        double p = 1 - r;
        double x = 0.0, y = r; //considera como se o centro fosse (0,R)

        while (x < y) {
            aux = new Coordenada();
            x++;
            if (p < 0.0) {
                p = p + 2 * x + 1;
            } else {
                y--;
                p = p + 2 * x + 1 - 2 * y;
            }

            aux.setCoordenada(y, x);
            octante1.add(aux);

        }

	/*lista de coordenadas da parte de baixo do circulo calculadas apartir do octante 1
         essa lista eh pra caso precise desenhar o meio infinito
        */
        
        //octante 4
        for (int i = 0; i < octante1.size(); i++) {
            aux = new Coordenada();
            aux.setCoordenada(-octante1.get(i).getCoordenadaX(), octante1.get(i).getCoordenadaY());
            listaCoord.add(aux);
        }

        //octante 3
        for (int i = octante1.size() - 1; i >= 0; i--) {
            aux = new Coordenada();
            aux.setCoordenada(-octante1.get(i).getCoordenadaY(), octante1.get(i).getCoordenadaX());
            listaCoord.add(aux);
        }

        //octante 2
        for (int i = 0; i < octante1.size(); i++) {
            aux = new Coordenada();
            aux.setCoordenada(octante1.get(i).getCoordenadaY(), octante1.get(i).getCoordenadaX());
            listaCoord.add(aux);
        }

        //octante 1
        for (int i = octante1.size() - 1; i >= 0; i--) {
            aux = new Coordenada();
            aux.setCoordenada(octante1.get(i).getCoordenadaX(), octante1.get(i).getCoordenadaY());
            listaCoord.add(aux);
        }

        return listaCoord;

    }
    
    /*
    Calcula os pontos da trajetoria dos steps, apartir do raio de dos octetos e salva no atributo
    trajetoria, para que esses pontos sejam usados para movimentar a imagem no metodo run()
    */
    public void calculaSteps() {
        ArrayList<Coordenada> traj = new ArrayList<Coordenada>();
        traj = calcTrajetoria(this.r);
        
        int octante = 0; //na verdade é o 5 octante
        Double stepsDouble = (Double) steps;
        Coordenada aux;

        double initialMatrix[] = new double[6];

        AffineTransform initialTransformation = new AffineTransform();
        initialTransformation.setToTranslation(xi + traj.get(0).getCoordenadaX() - (largura/2), yi + traj.get(0).getCoordenadaY() - (altura/2));
        initialTransformation.getMatrix(initialMatrix);

        double finalMatrix[] = new double[6];

        AffineTransform finalTransformation = new AffineTransform();
        finalTransformation.setToTranslation(xi + traj.get(octante).getCoordenadaX() - (largura/2), yi + traj.get(octante).getCoordenadaY() - (altura/2));
        finalTransformation.getMatrix(finalMatrix);

        AffineTransform intermediateTransform;

        for (int j = 0; j < 4; j++) {

            octante = octante - 1 + traj.size() / 4;

            finalTransformation = new AffineTransform();
            finalTransformation.setToTranslation(xi + traj.get(octante).getCoordenadaX() - (largura / 2), yi + traj.get(octante).getCoordenadaY() - (altura / 2));
            finalTransformation.getMatrix(finalMatrix);

            for (double i = 0; i <= stepsDouble; i++) {
                //transformacao intermediaria entre os angulos
                intermediateTransform = new AffineTransform(
                        convexCombination(initialMatrix, finalMatrix, i / stepsDouble));
                aux = new Coordenada();
                aux.setCoordenada((int) intermediateTransform.getTranslateX(), (int) intermediateTransform.getTranslateY());

                this.trajetoria.add(aux);
            }//fim for

            initialTransformation = new AffineTransform(finalTransformation);
            initialTransformation.getMatrix(initialMatrix);

        }//fim for

        //segundo circulo
        //altera o (x,y) central
        this.xi = xi + 2 * r;

        initialTransformation = new AffineTransform(finalTransformation);
        initialTransformation.rotate(0);
        initialTransformation.getMatrix(initialMatrix);

        octante = 0;

        for (int j = 0; j < 4; j++) {

            octante = octante - 1 + traj.size() / 4;

            finalTransformation = new AffineTransform();
            finalTransformation.setToTranslation(xi + traj.get(octante).getCoordenadaX() - (largura / 2), yi + traj.get(octante).getCoordenadaY() - (altura / 2));
            finalTransformation.getMatrix(finalMatrix);

            for (double i = 0; i <= stepsDouble; i++) {
                //transformacao intermediaria entre os angulos
                intermediateTransform = new AffineTransform(
                        convexCombination(initialMatrix, finalMatrix, i / (stepsDouble)));
                aux = new Coordenada();
                aux.setCoordenada((int) intermediateTransform.getTranslateX(), (int) intermediateTransform.getTranslateY());
                this.trajetoria.add(aux);
            }//fim for

            initialTransformation = new AffineTransform(finalTransformation);
            initialTransformation.getMatrix(initialMatrix);
        }//fim for

    }

    public void run() {
        int i;
	//verifica atraves de da lista de steps em que octeto está
        for ( i = 0 ; i< this.trajetoria.size()-1; i++){
           
            if (alpha>=0 && alpha<=1){

                if (i >= 0 && i <= 1)  mix = t1.mixWith(t2, alpha);             
                if (i >= steps && i <= (2*steps))  mix = t2.mixWith(t3, alpha);     
                if (i >= (2*steps) && i <= (3*steps))  mix = t3.mixWith(t4, alpha); 
                if (i >= (3*steps) && i <= (4*steps))  mix = t4.mixWith(t5, alpha); 
                if (i >= (4*steps) && i <= (5*steps))  mix = t5.mixWith(t6, alpha); 
                if (i >= (5*steps) && i <= (6*steps))  mix = t6.mixWith(t7, alpha); 
                if (i >= (6*steps) && i <= (7*steps))  mix = t7.mixWith(t8, alpha); 
                if (i >= (8*steps) && i <= (trajetoria.size()-1))  mix = t8.mixWith(t9, alpha); 

                buffid.g2dbi.drawImage(mix, (int) this.trajetoria.get(i).getCoordenadaX(), (int)this.trajetoria.get(i).getCoordenadaY(), null);
                buffid.repaint();
                                 
             }
            alpha = alpha + deltaAlpha;  
            if(alpha >=1 ) alpha = 0; // para trasformar a proxima imagem
            
        }
        
    }

    /**
     * Conmputes the convex combination of two vectors/points (given as
     * Java-Arrays).
     *
     * @param a initial point
     * @param b endpoint (must have the same length as the array a)
     * @param alpha Proportion that the initial point contributes to the convex
     * combination. soll. 0<=alpha<=1 must hold.
     *
     * @return The convex combination (1-alpha)*a + alpha*b of the two points.
     */
    public static double[] convexCombination(double[] a, double[] b, double alpha) {
        double[] result = new double[a.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = (1 - alpha) * a[i] + alpha * b[i];
        }

        return (result);
    }

}
