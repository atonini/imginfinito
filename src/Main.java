
import java.awt.image.BufferedImage;
import java.util.Scanner;
import java.util.Timer;

/**
 *
 * @author aline
 */
public class Main {
    
    public static void main(String[] argv){
	    
            int width;
	    int height;
	    long delay = 200;

            int r=0;
	    double steps=0;
	    
	    Scanner entrada = new Scanner(System.in);  
	    
	    while (r<=0){
	    	System.out.println("Digite o valor de R em inteiros :");
	    	r = entrada.nextInt();
	    }
	    
	    while(steps<=0){
	    	System.out.println("Digite o valor o numero de steps em double :");
	    	steps = entrada.nextDouble();
	    }
	    
	    entrada.close();
	    width =r*2+3*240;
	    height = r*4+3*200;

	    BufferedImage bi = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
	    
	    BufferedImageDrawer bid = new BufferedImageDrawer(bi,width,height);
            
       	    MorphRet mcs = new MorphRet(bid,r,steps);

            Timer t = new Timer();
	    t.scheduleAtFixedRate(mcs,0,delay);
	    
	  }
		
    
}
